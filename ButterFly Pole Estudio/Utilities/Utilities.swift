//
//  Utilities.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 20/02/24.
//

import Foundation



class Utilities {
    
    private init(){}
    
    public static var shared: Utilities = {
        Utilities()
    }()
    
    func timerAlert(work: @escaping ()-> Void, times: Double = 3.0) {
        DispatchQueue.main.asyncAfter(deadline: .now() + times) {
            work()
        }
    }
}
