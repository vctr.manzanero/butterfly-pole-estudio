//
//  MessageClouds.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 29/04/24.
//

import SwiftUI
import FirebaseStorage

struct DinamicHeight: PreferenceKey {
   static var defaultValue: CGFloat { 0 }
   
   static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
      value = nextValue()
   }
}

struct DynamicWidth: PreferenceKey {
   static var defaultValue: CGFloat { 0 }
   
   static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
      value = nextValue() + 10
   }
}

enum DirectionMessage {
    case right
    case left
}

struct MessageClouds: View {
    
    //var direction: DirectionMessage+
    //var timeStamp: Double
    @State var textHeight: CGFloat = 0
    @State var textWidth: CGFloat = 0
    //var text: String = ""
    //var sent = true, received = true, seen = true
    //var type: String
    //var url: String
    @State var stateUrl: URL?
    @State private var imageURL: URL?
    var msj: Message
    
    var body: some View {
        
        switch msj.direction {
        case .right:
            
            if msj.type.elementsEqual("txt") {
                RightMsj(timeStamp: msj.timestamp, text: msj.text)
            } else {
                HStack{
                    Spacer()
                        .foregroundStyle(.clear)
                    AsyncImage(url: stateUrl, scale: 0.15) { phase in // 1
                        if let image = phase.image { // 2
                            // if the image is valid
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                
                        } else if phase.error != nil { // 3
                            // some kind of error appears
                            Text("No image available")
                        } else {
                            //appears as placeholder image
                            Image(systemName: "photo") // 4
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                        }
                    }.frame(width: 150, height: 150)
                        .cornerRadius(5)
                        .padding()
                }
                .listRowBackground(Color.clear)
                .listRowSeparator(.hidden)
                .listRowSpacing(0)
                .task {
                    downloadImage(url: msj.url)
                }
            }
        case .left:
            if msj.type.elementsEqual("txt") {
                HStack {
                    ZStack {
                        UnevenRoundedRectangle(topLeadingRadius: 10, bottomLeadingRadius: 0, bottomTrailingRadius: 10, topTrailingRadius: 10)
                            . frame(width: textWidth,  height: textHeight)
                            .foregroundColor(.white)
                        HStack (spacing: 0){
                                
                            Text(DateTimeStamp.getStringTime(timeStamp: msj.timestamp))
                                .fontWeight(.light)
                                .font(.system(size: 8))
                                .padding()
                        }
                        .offset(x: -textWidth / 2 + 55, y: textHeight / 2 - 6)
                        Text(msj.text)
                            .multilineTextAlignment(.trailing)
                            .overlay(
                                GeometryReader { proxy in
                                    Color
                                        .clear
                                        .preference(key: DinamicHeight.self,
                                                    value: proxy.size.height)
                                        .preference(key: DynamicWidth.self,
                                                    value: proxy.size.width)
                                }
                            ).padding(5)
                            .onPreferenceChange(DinamicHeight.self) { value in
                                DispatchQueue.main.async {
                                    self.textHeight = value + 20
                                }
                            }
                            .onPreferenceChange(DynamicWidth.self) { value in
                                DispatchQueue.main.async {
                                    if value < 100 {
                                        self.textWidth = 125
                                    } else  {
                                        self.textWidth = value + 20
                                    }
                                }
                            }
                    }
                    Spacer()
                }
                .listRowBackground(Color.clear)
                .listRowSeparator(.hidden)
                .listRowSpacing(0)
            } else {
                HStack{
                    VStack {
                        AsyncImage(url: stateUrl, scale: 0.01) { phase in // 1
                            if let image = phase.image { // 2
                                // if the image is valid
                                image
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .onTapGesture {
                                        UIApplication.shared.open(stateUrl ?? URL(string: "www.google.com")!)
                                    }
                                    
                            } else if phase.error != nil { // 3
                                // some kind of error appears
                                Text("No image available")
                            } else {
                                //appears as placeholder image
                                Image(systemName: "photo") // 4
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                            }
                        }
                        .frame(width: 150, height: 150)
                            .cornerRadius(5)
                            .padding()
                        Text(DateTimeStamp.getStringTime(timeStamp: msj.timestamp))
                            .fontWeight(.light)
                            .font(.system(size: 8))
                    }
                    Spacer()
                        .foregroundStyle(.clear)
                }
                .padding()
                .listRowBackground(Color.clear)
                .listRowSeparator(.hidden)
                .listRowSpacing(0)
                .task {
                    downloadImage(url: msj.url)
                }
            }
        }
        
    }
    
    func downloadImage(url: String) {
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let starsRef = storageRef.child("imageChat/\(url).jpg")

        // Fetch the download URL
        starsRef.downloadURL { url, error in
            if error != nil {
            fatalError("Error no encontrado")
          } else {
              self.stateUrl = url
          }
        }
    }
}

struct Messageright: View {
    
    @Binding var textHeight: CGFloat
    @Binding var textWidth: CGFloat
    
    var body: some View {
        HStack(spacing: 0) {
            UnevenRoundedRectangle(topLeadingRadius: 10, bottomLeadingRadius: 10, bottomTrailingRadius: 0, topTrailingRadius: 10)
                . frame(width: textWidth,  height: textHeight)
                .foregroundColor(Color(red: 220/255, green: 248/255, blue: 198/255))
            Triangle()
                .foregroundColor(Color(red: 220/255, green: 248/255, blue: 198/255))
                .frame(width: 20, height: 20)
                .rotationEffect(.degrees(90))
        }
    }
    
}

struct Triangle: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        return path
    }
}



/*#Preview {
    //MessageClouds(direction: .left, timeStamp: 1715810931.521379, text: "h",sent: true, received: true, seen: true, type: "txt", url: "6FA6C9E8-5908-4C7C-A4D3-080FDFC17673.jpg")
    //Messageright(textHeight: .constant(100), textWidth: .constant(300))
}*/
