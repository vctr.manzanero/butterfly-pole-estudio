//
//  CheckMarks.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 16/05/24.
//

import SwiftUI

struct checkmarks: View {
    
    var sent: Bool
    var received: Bool
    var seen: Bool
    
    var body: some View {
        
        if sent && received && seen {
            ZStack {
                Image(systemName: "checkmark")
                    .resizable()
                    .frame(width: 10, height: 10)
                Image(systemName: "checkmark")
                    .resizable()
                    .frame(width: 10, height: 10)
                    .offset(x: 4)
            }
            .foregroundStyle(Color.blue)
            .offset( y: -10)
        } else if sent && received && !seen {
            ZStack {
                Image(systemName: "checkmark")
                    .resizable()
                    .frame(width: 10, height: 10)
                Image(systemName: "checkmark")
                    .resizable()
                    .frame(width: 10, height: 10)
                    .offset(x: 4)
            }
            .foregroundStyle(Color.gray)
            .offset( y: -10)
        } else if sent && !received && !seen {
            ZStack {
                Image(systemName: "checkmark")
                    .resizable()
                    .frame(width: 10, height: 10)
            }
            .foregroundStyle(Color.gray)
            .offset( y: -10)
        }
        
    }
    
}
