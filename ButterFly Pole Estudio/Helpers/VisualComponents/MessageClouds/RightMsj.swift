//
//  RightMsj.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 17/05/24.
//

import SwiftUI

struct RightMsj: View {
    @State var textHeight: CGFloat = 0
    @State var textWidth: CGFloat = 0
    var sent = true, received = true, seen = true
    var timeStamp: Double
    var text: String
    
    var body: some View {
        HStack {
            Spacer()
            ZStack {
                
                UnevenRoundedRectangle(topLeadingRadius: 10, bottomLeadingRadius: 10, bottomTrailingRadius: 0, topTrailingRadius: 10)
                    . frame(width: textWidth,  height: textHeight)
                    .foregroundColor(Color(red: 220/255, green: 248/255, blue: 198/255))
                
                HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 0) {
                    Text(DateTimeStamp.getStringTime(timeStamp: timeStamp))
                        .fontWeight(.light)
                        .font(.system(size: 8))
                        .padding()
                    checkmarks(sent: sent, received: received, seen: seen)
                        .padding()
                        .offset(x:-25 ,y: 8)
                        
                }
                .offset(x: textWidth / 2 - 50, y: textHeight / 2 - 6)
                Text(text)
                    .multilineTextAlignment(.trailing)
                    .overlay(
                        GeometryReader { proxy in
                            Color
                                .clear
                                .preference(key: DinamicHeight.self,
                                            value: proxy.size.height)
                                .preference(key: DynamicWidth.self,
                                            value: proxy.size.width )
                        }
                    )
                    .padding(10)
                    .onPreferenceChange(DinamicHeight.self) { value in
                        DispatchQueue.main.async {
                            self.textHeight = value + 20
                        }
                    }
                    .onPreferenceChange(DynamicWidth.self) { value in
                        DispatchQueue.main.async {
                            if value < 100 {
                                self.textWidth = 125
                            } else  {
                                self.textWidth = value + 20
                            }
                            
                        }
                    }
                
            }
            
        }
        .listRowBackground(Color.clear)
        .listRowSeparator(.hidden)
        .listRowSpacing(0)
    }
}

