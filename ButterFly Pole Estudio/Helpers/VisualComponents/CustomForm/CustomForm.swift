//
//  CustomForm.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 11/02/24.
//

import SwiftUI

enum CustomFormStyle {
    case standar
    case secure
    case bold
    case noEmpty
}

struct CustomForm: View {
    
    var title: String = ""
    @Binding var stateName: String
    var style: CustomFormStyle = .standar
    var  placeHolder: String = ""
    
    var body: some View {
        VStack {
            HStack {
                Text(title)
                    .font(.subheadline)
                    .foregroundStyle(.secondary)
                    .padding(.leading, 20)
                Spacer()
            }
            HStack {
                switch style {
                case .noEmpty:
                    TextField("Campo requerido", text: $stateName)
                        .frame(height: 10)
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 20).fill(Color(red: 237/255, green: 237/255, blue: 237/255)))
                        .overlay(
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(Color.red)
                        )
                case .standar:
                    TextField("", text: $stateName)
                        .frame(height: 10)
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 20).fill(Color(red: 237/255, green: 237/255, blue: 237/255)))
                        .foregroundColor(.black)
                case .secure:
                    SecureField("", text: $stateName)
                        .frame(height: 10)
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 20).fill(Color(red: 237/255, green: 237/255, blue: 237/255)))
                        .foregroundColor(.black)
                case .bold:
                    TextField(placeHolder, text: $stateName)
                        .frame(height: 10)
                        .padding()
                        .overlay(
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(Color.black)
                        )
                    
                }
            }
        }
        .padding()
    }
}

#Preview {
    CustomForm(title: "", stateName: .constant(""), style: .noEmpty, placeHolder: "nombre")
}


