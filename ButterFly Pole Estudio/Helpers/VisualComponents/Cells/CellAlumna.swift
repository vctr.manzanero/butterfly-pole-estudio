//
//  CellAlumna.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 12/02/24.
//

import SwiftUI

struct CellAlumna: View {
    var body: some View {
        HStack {
            Image(systemName: "person.circle.fill")
                .resizable()
                .frame(width: 50, height: 50)
                .padding()
            VStack (alignment: .leading){
                Text("Clase muestra")
                    .font(.headline)
                    .bold()
                Text("nombre")
                    .font(.subheadline)
                    .fontWeight(.light)
            }
            Spacer()
            Image(systemName: "plus.circle.fill")
                .resizable()
                .frame(width: 40, height: 40)
                .foregroundStyle(.princilal)
                .padding()
        }
        .clipShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
        .padding()
    }
}

#Preview {
    CellAlumna()
}
