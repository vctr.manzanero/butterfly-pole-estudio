//
//  headers.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 12/02/24.
//

import SwiftUI
import UIKit

struct Headers: View {
    
    var title: String = "Title"
    var subTitle: String =  "SubTitle"
    var imagen: Image = Image(systemName: "person.circle.fill")
    @State var buscador: String = ""
    
    var body: some View  {
        VStack {
            HStack {
                imagen
                    .resizable()
                    .frame(width: 50, height: 50)
                    .padding()
                VStack (alignment: .leading){
                    Text(title)
                        .font(Font.system(size: 25))
                        .foregroundStyle(.white)
                        .font(.headline)
                        .bold()
                    Text(subTitle)
                        .font(Font.system(size: 20))
                        .foregroundStyle(.white)
                        .font(.subheadline)
                        .fontWeight(.light)
                }
                Spacer()
            }
        }
        .frame(height: 150)
        .background(.princilal)
        .clipShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
        .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5))
    }
}

#Preview {
    Headers()
}

