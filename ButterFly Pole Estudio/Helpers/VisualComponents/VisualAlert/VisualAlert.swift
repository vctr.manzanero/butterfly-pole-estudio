//
//  VisualAlert.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 14/02/24.
//

import SwiftUI

enum typeAlert {
    case succes
    case errors
}

struct VisualAlert: View {
    
    var text: String
    var style: typeAlert
    
    var body: some View {
        VStack {
            Spacer()
            switch style {
            case .succes:
                HStack {
                    Spacer()
                    Text(text)
                        .offset(y: 10)
                        .font(.system(size: 20))
                        .fontWeight(.bold)
                        .foregroundStyle(.white)
                    Spacer()
                }
                .background(.blue)
            case .errors:
                HStack {
                    Spacer()
                    Text(text)
                        .offset(y: 10)
                        .font(.system(size: 20))
                        .fontWeight(.bold)
                        .foregroundStyle(.white)
                    Spacer()
                }
                .background(.red)
            }
            
        }
    }
}

#Preview {
    VisualAlert(text: "Hola mundo", style: .errors)
}
