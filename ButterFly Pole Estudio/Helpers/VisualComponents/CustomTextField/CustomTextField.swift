//
//  CustomTextField.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 11/02/24.
//

import SwiftUI

enum CustomTextFieldStyle {
    case standar
    case secure
}

struct CustomTextField: View {
    
    @Binding var custonState: String
    var text: String
    var style: CustomTextFieldStyle
    
    var body: some View {
        switch style {
        case .standar:
            TextField(text, text: $custonState)
                .frame(height: 44)
                .keyboardType(.emailAddress)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .textFieldStyle(RoundedBorderTextFieldStyle())
        case .secure:
            SecureField(text, text: $custonState)
                .frame(height: 44)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .textFieldStyle(RoundedBorderTextFieldStyle())
        }
    }
}

#Preview {
    CustomTextField(custonState: .constant(""), text: "", style:  .standar)
}
