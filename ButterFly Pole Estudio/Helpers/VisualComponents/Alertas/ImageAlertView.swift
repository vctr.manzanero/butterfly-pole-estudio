//
//  ImageAlertView.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 17/05/24.
//

import SwiftUI

struct ImageAlertView: View {
    @Binding var image: Image?
    @Binding var closed: Bool
    @ObservedObject var viewModel: ChatViewModel
    var reciber: String

    var body: some View {
        VStack {
            if #available(iOS 16.4, *) {
                image?
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 250, height: 250)
                    .presentationCornerRadius(50)
                    .padding()
            } else {
                image?
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 250, height: 250)
                    .padding()
            }

            Text("¡Imagen Seleccionada!")
                .font(.headline)
                .padding()

            Text("Seleccione aceptar si desea enviarla")
                .multilineTextAlignment(.center)
                .padding()
            
            HStack {
                Button("Aceptar") {
                    // Cerrar la modal
                    viewModel.sendMessageImage(receiber: reciber, image: image?.asUIImage() ?? UIImage())
                    dismiss()
                }
                .padding()

                Button("Cerrar") {
                    // Cerrar la modal
                    dismiss()
                }
                .padding()
            }
        }
        .padding()
    }

    private func dismiss() {
        closed = false
    }
}
