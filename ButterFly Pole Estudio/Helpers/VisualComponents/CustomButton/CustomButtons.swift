//
//  CustomButtons.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 11/02/24.
//

import SwiftUI

enum ButtonStyles {
    case principal
    case withIcon
}

struct Custom_Buttons: View {
    var title: String = ""
    var style: ButtonStyles
    var nameImage: String = ""
    
    var body: some View {
        switch style {
        case .principal:
            ZStack {
                RoundedRectangle(cornerSize: CGSize(width: 80, height: 40))
                    .frame(height: 40)
                    .foregroundStyle(.princilal)
                Text(title)
                    .foregroundStyle(.white)
                    .font(.headline)
                    .fontWeight(.bold)
            }
        case .withIcon:
            Image(systemName: nameImage)
                .resizable()
                .frame(width: 20, height: 20)
                .font(.largeTitle.weight(.bold))
                .aspectRatio(/*@START_MENU_TOKEN@*/1.5/*@END_MENU_TOKEN@*/, contentMode: .fit)
                .foregroundStyle(.princilal)
        }
        
    }
}

#Preview {
    Custom_Buttons(title: "Example", style: .principal)
}
