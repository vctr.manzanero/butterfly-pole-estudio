//
//  Localizable.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 11/02/24.
//

import Foundation

enum Strings {
    case userName
    case userMain
    case userPass
    case userDate
    case userNumber
    case textSave
    case noneString
}

extension String {
    
    func textLabel(identifier: Strings) -> String {
        switch identifier {
        case .userName:
            return "INTRODUCE TU NOMBRE"
        case .userMain:
            return "INGRESA TU CORREO ELECTRÓNICO"
        case .userPass:
            return "INTRODUCE TU CONTRASEÑA"
        case .userDate:
            return "INTRODUCE TU FECHA DE NACIMIENTO"
        case .userNumber:
            return "INTRODUCE TU NUMERO DE TELÉFONO"
        case .textSave:
            return "Guardar"
        case .noneString:
            return ""
        }
    }
    
}
