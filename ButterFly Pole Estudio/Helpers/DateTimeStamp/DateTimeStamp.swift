//
//  DateTimeStamp.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 15/05/24.
//

import Foundation

class DateTimeStamp {
    
    static func getStringTime(timeStamp: Double) -> String {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM/d/YYYY hh:mm a"
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
}
