//
//  ServicesProtocols.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 13/02/24.
//

import UIKit

protocol GetData: AnyObject {
    func addUser(email: String, pass: String, success: @escaping () -> Void, error:  @escaping () -> Void )
}

protocol getLogin: AnyObject {
    func loginUser(email: String,
                   pass: String,
                   success: @escaping (String) -> Void,
                   error:  @escaping () -> Void )
    
    func getInfoUser(uid: String) async -> Int
    
}

protocol getInfoUser: AnyObject {
    func getUser (data: String,
                        success: @escaping (User) -> Void,
                        errorResponse: @escaping () -> Void)  async
}

protocol uploadFireStore {
    func upload(image: UIImage,
                success: @escaping () -> Void,
                errorResponse: @escaping () -> Void,
                completion: @escaping () -> Void)
}


protocol GetclassPole {
    func GetclassPole( success: @escaping ([PoleClass]) -> Void,
                       errorResponse: @escaping () -> Void,
                       completion: @escaping () -> Void) async
}
