//
//  TemplateView.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 12/02/24.
//

import SwiftUI

struct TemplateView <Content: View>: View {
    
    @ViewBuilder let content: Content
    
    var body: some View {
        VStack {
            Headers()
            GeometryReader{ screen in
                HStack {
                    VStack {
                        content
                    }
                    .frame(width: screen.size.width * 1.0, height: screen.size.height)
                    .background(.back)
                }
                .clipShape(RoundedRectangle(cornerSize: CGSize(width: 20, height: 10)))
                .offset(y: -50)
            }
            .padding()
        }
        .background(.back2)
        .ignoresSafeArea()
    }
}


#Preview {
    TemplateView {
        Text("45f")
    }
}
