//
//  ChatModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 27/04/24.
//

import Foundation

enum msjType {
    case text
    case image
}

struct ChatModel: Identifiable {
    var name: String
    var hora: String
    var id: Int
    var image: String
}
