//
//  PoleClass.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 22/02/24.
//

import SwiftUI

struct PoleClass: Identifiable {
    var name: String
    var hora: String
    var id: Int
    var isSelect: Bool
}
