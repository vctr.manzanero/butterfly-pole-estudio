//
//  Message.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 15/05/24.
//

import Foundation

struct Message: Identifiable, Equatable, Hashable {
    var id: String
    var text: String
    var senderID: String
    var timestamp: Double
    var direction: DirectionMessage
    var isRead: Bool
    var url: String = ""
    var type: String
    
    static func == (lhs: Message, rhs: Message) -> Bool {

        return lhs.id == rhs.id 
    }
}

struct CountsUser: Identifiable, Equatable {
    var email: String
    var name: String
    var id:  String
}
