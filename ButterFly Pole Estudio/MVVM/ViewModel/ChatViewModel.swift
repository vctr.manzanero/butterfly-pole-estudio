//
//  ChatViewModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 27/04/24.
//

import Foundation
import FirebaseDatabaseInternal
import SwiftUI
import FirebaseStorage
import PDFKit

import UniformTypeIdentifiers

class ChatViewModel: ObservableObject {
    @Published var listCharUser: [Message] = []
    var ref: DatabaseReference!
    var useCase: UseCaseRealtimeDataBase = UseCaseRealtimeDataBase()
    private var handle: DatabaseHandle?
    
    
    @State var selectedFile: URL?
    @State var uploadProgress: Double = 0.0
    @State var isUploading = false
    
    func readMessages(uidReceiver: String) {
        
        ref = Database.database().reference()
        var newMessages: [Message] = []
        handle = ref.child("messages").observe(.value) { snapshot in
            for child in snapshot.children {
                if let childSnapshot = child as? DataSnapshot,
                   var dict = childSnapshot.value as? [String: Any],
                   let text = dict["text"] as? String,
                   let senderID = dict["senderID"] as? String,
                   let read = dict["isRead"] as? Bool,
                   let timestamp = dict["timestamp"] as? Double,
                   let receiverID = dict["receiver"] as? String,
                   let type = dict["type"] as? String,
                   let url = dict["url"] as? String {
                   var direccion: DirectionMessage?
                    
                    if ( (receiverID.elementsEqual(uidReceiver) || receiverID.elementsEqual(UserDefaults.standard.string(forKey: "idUser") ?? "") ) && ( senderID.elementsEqual(uidReceiver) || senderID.elementsEqual(UserDefaults.standard.string(forKey: "idUser") ?? "" ) ) ){
                        if senderID.elementsEqual(UserDefaults.standard.string(forKey: "idUser") ?? "") {
                            direccion = .right
                        } else  {
                            direccion = .left
                        }
                        if ( receiverID.elementsEqual(UserDefaults.standard.string(forKey: "idUser") ?? "") && !read ) {
                            dict.updateValue(true, forKey: "isRead")
                            self.ref.child("messages").child(childSnapshot.key).updateChildValues(dict)
                            let message = Message(id: childSnapshot.key, text: text, senderID: senderID, timestamp: timestamp, direction: direccion ?? .right, isRead: true, url: url, type: type)
                            newMessages.append(message)
                        } else {
                            let message = Message(id: childSnapshot.key, text: text, senderID: senderID, timestamp: timestamp, direction: direccion ?? .right, isRead: read, url: url, type: type)
                            newMessages.removeAll { $0.id == childSnapshot.key }
                            newMessages.append(message)
                        }
                        
                    }
                                                                                                                                                    
                }
            }
            self.listCharUser = Array(Set(newMessages)).sorted(by: { $0.timestamp < $1.timestamp })
        }
    }
    
    func stopObservingMessages() {
        if let handle = handle {
            ref.removeObserver(withHandle: handle)
        }
    }
    
    func sendMessage(msj: String, receiber: String) {
        useCase.sendMessage(newMessageText: msj, idChat: receiber)
    }
    
    func sendMessageImage(receiber: String, image: UIImage) {
        useCase.sendMessageImage(idChat: receiber, image: image)
    }
    
    func selectPDF() {
        let documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: [UTType.pdf])
        documentPicker.allowsMultipleSelection = false
        documentPicker.delegate = self
        UIApplication.shared.windows.first?.rootViewController?.present(documentPicker, animated: true, completion: nil)
    }
    
    struct PDFKitRepresentedView: UIViewRepresentable {
        let url: URL

        init(_ url: URL) {
            self.url = url
        }

        func makeUIView(context: Context) -> PDFView {
            PDFView()
        }

        func updateUIView(_ uiView: PDFView, context: Context) {
            uiView.document = PDFDocument(url: url)
        }
    }
    
     func uploadPDF() {
        guard let selectedFile = selectedFile else { return }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let pdfRef = storageRef.child("pdf/\(UUID().uuidString).pdf")
        
        isUploading = true
        
        let task = pdfRef.putFile(from: selectedFile, metadata: nil) { metadata, error in
            self.isUploading = false
            
            if let error = error {
                print("Error al subir el archivo: \(error.localizedDescription)")
                return
            }
            
            print("Archivo subido exitosamente")
            
            // Puedes manejar la URL del archivo subido aquí si es necesario
            pdfRef.downloadURL { url, error in
                if let downloadURL = url {
                    print("URL de descarga: \(downloadURL)")
                } else {
                    print("Error al obtener la URL de descarga: \(error?.localizedDescription ?? "Unknown error")")
                }
            }
        }
        
        task.observe(.progress) { snapshot in
            guard let progress = snapshot.progress else { return }
            self.uploadProgress = Double(progress.completedUnitCount) / Double(progress.totalUnitCount) * 100
        }
    }
    
}


extension  ChatViewModel: UIDocumentPickerDelegate {
    var superclass: AnyClass? {
       nil
    }
    
    func isEqual(_ object: Any?) -> Bool {
        true
    }
    
    var hash: Int {
        0
    }
    
    
    func `self`() -> Self {
        self
    }
    
    func perform(_ aSelector: Selector!) -> Unmanaged<AnyObject>! {
        nil
    }
    
    func perform(_ aSelector: Selector!, with object: Any!) -> Unmanaged<AnyObject>! {
        nil
    }
    
    func perform(_ aSelector: Selector!, with object1: Any!, with object2: Any!) -> Unmanaged<AnyObject>! {
        nil
    }
    
    func isProxy() -> Bool {
        true
    }
    
    func isKind(of aClass: AnyClass) -> Bool {
        true
    }
    
    func isMember(of aClass: AnyClass) -> Bool {
        true
    }
    
    func conforms(to aProtocol: Protocol) -> Bool {
        true
    }
    
    func responds(to aSelector: Selector!) -> Bool {
        true
    }
    
    var description: String {
        ""
    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let selectedFileURL = urls.first else { return }
        selectedFile = selectedFileURL
    }
}
