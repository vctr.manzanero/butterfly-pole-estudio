//
//  ChatListViewModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 16/05/24.
//

import Foundation

class ChatListViewModel: ObservableObject {
    
    var useCase = UseCaseFireBase()
    @Published var list: [CountsUser] = []
    
    @MainActor
    func getUsersChat() async {
        self.list = await useCase.getListUsers()
    }
    
}
