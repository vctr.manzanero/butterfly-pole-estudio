//
//  RegisterViewModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 14/02/24.
//

import Firebase
import SwiftUI

class RegisterViewModel: ObservableObject {
    
    var useCase: UseCaseFireStore
    @Published var showingAlert: Bool = false
    @Published var isShowPicker =  false
    @Published var loadin: Bool = false
    @Published var response: ResponseApi = .none
    @Published var formsUse:[CustomFormStyle] = [.standar, .standar,.secure,.standar,.standar]
    
    init(useCase: UseCaseFireStore = UseCaseFireStore()) {
        self.useCase = useCase
    }
    
    func saveDataUser(_ datas: [String] ) {
        
        var noEmpty: Bool = false
        
        for i in 0..<datas.count {
            if datas[i].elementsEqual("") {
                formsUse[i] = .noEmpty
            } else {
                formsUse[i] = .standar
            }
        }
        
        for i in 0..<formsUse.count {
            if formsUse[i] == .noEmpty {
                noEmpty = true
            }
        }
        
        if !noEmpty {
            self.useCase.insertStuden(data: datas,
                                      success: {
                self.loadin = false
                self.response = .success
                self.showingAlert = true
            },
                                      errorResponse: {
                self.loadin = false
                self.response = .errrors
            }
            )
        }
    }
    
    func sendImage(_ imageProfile: UIImage, datas: [String]) {
        self.loadin = true
        self.useCase.upload(image: imageProfile, success: { [weak self] in
            self?.response = .success
        }, errorResponse: {
            self.response = .errrors
        }, completion: { [weak self] in
            self?.saveDataUser(datas)
        })
    }
    
    @MainActor
    func viewMessage() {
        Utilities.shared.timerAlert(work:{
            self.response = .none
        })
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}$", options: [.caseInsensitive])
        let resonse =  regex.firstMatch(in: email, options: [], range: NSRange(location: 0, length: email.utf16.count)) != nil
        if resonse {
            return resonse
        } else {
            self.formsUse[1] = .noEmpty
            return resonse
        }
    }
    
}
