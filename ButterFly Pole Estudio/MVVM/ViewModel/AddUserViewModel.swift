//
//  AddUserViewModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 13/02/24.
//

import Foundation


class AddUserViewModel: ObservableObject {
    
    @Published var success: Bool = false
    @Published var error: Bool = false
    var useCaseFireBase: UseCaseFireBase = UseCaseFireBase()
    
    func addUser (email: String, pass: String) {
        useCaseFireBase.addUser(email: email, pass: pass, success: { [weak self] in
            self?.success = true
            self?.viewMessage()
        }, error: { [weak self] in
            self?.error = true
            self?.viewMessage()
        })
    }
    
    func viewMessage() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.success = false
            self.error = false
        }
    }
}
