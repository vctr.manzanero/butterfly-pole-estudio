//
//  CalendarViewModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 20/02/24.
//

import Foundation
import EventKit
import UIKit
import SwiftUI
import FirebaseStorage

@MainActor
class CalendarViewModel: ObservableObject {
    
    let storage = Storage.storage()
    private let eventStore = EKEventStore()
    //@Published var createdEvent: EKEvent?
    
    @Published var responseError: Bool = false
    @Published var profile: Image = Image(systemName: "person.fill")
    @Published var loadingClass: Bool = false
    @Published var arrayClass: [PoleClass] = []
    @Published var dataUser: User = User(birtsDay: "", name: "", email: "", phone: "", uid: "", cretid: 0)
    
    var useCase: UseCaseFireBase = UseCaseFireBase()
    
    @MainActor
    func getInfoUser() {
        Task {
            await self.useCase.getUser(data: UserDefaults.standard.string(forKey: "idUser") ?? "",
                                       success: { response in
                DispatchQueue.main.async {
                    self.dataUser = response
                }
            },
                                       errorResponse: {
                DispatchQueue.main.async {
                    self.responseError = true
                }
            })
        }
    }
    
    @MainActor
    func getClass() {
        Task {
            self.loadingClass = true
            await self.useCase.GetclassPole(success: { response in
                self.arrayClass = response
                
            },
                                            errorResponse: {
                
            }, completion: {
                DispatchQueue.main.async {
                    self.loadingClass = false
                }
            })
        }
    }
    
    @MainActor
    func getImagen() {
        let storageRef = storage.reference().child("profiles/\(UserDefaults.standard.string(forKey: "idUser") ?? "").jpg")
        storageRef.getData(maxSize: 2370000) { data, error in
            if error == nil  {
                self.getInfoUser()
                self.createImage(data!)
            }
        }
    }
    
    @MainActor
    func saveDateClass(_ date: Date) {
        print(date)
    }
    
    @MainActor
    func createImage(_ value: Data) {
#if canImport(UIKit)
        let songArtwork: UIImage = UIImage(data: value) ?? UIImage()
        self.profile = Image(uiImage: songArtwork)
#elseif canImport(AppKit)
        let songArtwork: NSImage = NSImage(data: value) ?? NSImage()
        self.profile = Image(nsImage: songArtwork)
#else
        self.profile = Image(systemImage: "some_default")
#endif
    }
    
    /*func createCalendarEvent() {
     // STEP 1
     eventStore.requestAccess(to: .event) { (granted, error) in
     if granted && error == nil {
     // STEP 2
     let event = EKEvent(eventStore: self.eventStore)
     event.title = "The guide worked 😮"
     event.startDate = Date()
     event.endDate = Date().addingTimeInterval(7200)
     
     event.notes = "Follow more guides on softwareanders.com and create more apps."
     
     // STEP 3
     event.calendar = self.eventStore.defaultCalendarForNewEvents
     
     // STEP 4
     do {
     try self.eventStore.save(event, span: .thisEvent)
     // STEP 5
     self.createdEvent = event
     } catch {
     print("Error saving event: \(error.localizedDescription)")
     }
     } else {
     print("Access to calendar not granted or an error occurred.")
     }
     }
     }*/
    
}
