//
//  LoginUserViewModel.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 14/02/24.
//

import Foundation

enum flowType: Int {
    case noFirst
    case firstFlow
    case none
}

class LoginUserViewModel: ObservableObject {
    
    var useCase = UseCaseFireBase()
    @Published var loading: Bool = false
    @Published var flow: flowType = .none
    @Published var noLogin: Bool = false
    
    func getLogin(email: String, pass: String) {
        loading = true
        useCase.loginUser(email: email, pass: pass, success: { [weak self] response in
            DispatchQueue.main.async {
                self?.getInfoUser(uid: response)
            }
        }, error: { [weak self] in
            DispatchQueue.main.async {
                self?.loading = false
                self?.noLogin = true
                Utilities.shared.timerAlert(work: {
                    self?.noLogin = false
                })
            }
        })
    }
    
    @MainActor
    func getInfoUser(uid: String)  {
        Task {
            flow = await flowType(rawValue: useCase.getInfoUser(uid: uid)) ?? .none
        }
        loading = false
    }
}
