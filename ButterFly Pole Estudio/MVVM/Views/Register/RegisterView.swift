//
//  RegisterView.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 11/02/24.
//

import SwiftUI

struct RegisterView: View {
    
    @State var isShowPicker: Bool = false
    @State var navigate: Bool = false
    @State var image: Image? = Image(systemName: "person.circle.fill")
    @State var name: String = ""
    @State var email: String = ""
    @State var pass: String = ""
    @State var birtsDay: String = ""
    @State var phone: String = ""
    @State var date = Date()
    @StateObject var viewModel: RegisterViewModel = RegisterViewModel()
    let dateFormatter = DateFormatter()
    
    var body: some View {
        
        if viewModel.loadin {
            ProgressView("Cargando")
        } else {
            if navigate {
                Login()
            } else {
                VStack {
                    VStack {
                        GeometryReader { screen in
                            VStack {
                                ZStack {
                                    Rectangle()
                                        .foregroundStyle (
                                            LinearGradient (
                                                colors: [.secondary1, .princilal],
                                                startPoint: .leading,
                                                endPoint: .trailing
                                            )
                                        )
                                    
                                    VStack {
                                        Text("!Bienvenidas¡")
                                            .font(.system(size: 30))
                                            .fontWeight(.bold)
                                        Text("Proceso de inscripción")
                                    }
                                    .foregroundStyle(.white)
                                    .offset(y: 20)
                                }
                                .frame(width: screen.size.width, height: screen.size.height * 0.15)
                                VStack {
                                    image?
                                        .resizable()
                                        .clipShape(Circle())
                                        .frame(width: 80, height: 80)
                                        .overlay(Circle().stroke(.white, lineWidth: 2))
                                        .shadow(radius: 7)
                                }
                                HStack (spacing: 80) {
                                    Button(action: {
                                        withAnimation {
                                            self.isShowPicker.toggle()
                                        }
                                    }, label: {
                                        Custom_Buttons(style: .withIcon, nameImage: "camera")
                                    })
                                    Button(action: {}, label: {
                                        Custom_Buttons(style: .withIcon, nameImage: "square.and.arrow.up")
                                    })
                                }
                                ScrollView {
                                    VStack (spacing: -10) {
                                        CustomForm(title: "".textLabel(identifier: .userName), stateName: $name, style: viewModel.formsUse[0])
                                        CustomForm(title: "".textLabel(identifier: .userMain), stateName: $email,  style: viewModel.formsUse[1])
                                        CustomForm(title: "".textLabel(identifier: .userPass), stateName: $pass, style: viewModel.formsUse[2])
                                        VStack {
                                            Text("".textLabel(identifier: .userDate))
                                                .font(.subheadline)
                                                .foregroundStyle(.secondary)
                                                .padding(.leading, -35)
                                            DatePicker("", selection: $date, displayedComponents: .date)
                                                .datePickerStyle(CompactDatePickerStyle())
                                                         .labelsHidden()
                                                         .padding(.top , 10)
                                        }
                                        CustomForm(title: "".textLabel(identifier: .userNumber), stateName: $phone, style: viewModel.formsUse[4])
                                        Button(action:  {
                                            dateFormatter.dateFormat = "y, M d"
                                            if self.viewModel.isValidEmail(email) {
                                                viewModel.sendImage(image?.asUIImage() ?? UIImage()
                                                                    , datas: [name,email,pass, dateFormatter.string(from: date), phone])
                                            }
                                        }, label: {
                                            Custom_Buttons(title: "".textLabel(identifier: .textSave), style: .principal)
                                                .padding()
                                        })
                                    }
                                }
                            }
                        }
                        
                    }
                    .ignoresSafeArea()
                    .alert("Debe Iniciar sesion con su nueva contraseña", isPresented: $viewModel.showingAlert) {
                        Button("OK", role: .cancel) {
                            self.navigate = true
                        }
                    }
                    .sheet(isPresented: $isShowPicker) {
                        ImagePicker(image: self.$image)
                    }
                    switch viewModel.response {
                    case .success:
                        VisualAlert(text: "Registrado con exito", style: .succes)
                            .frame(height: 10)
                    case .errrors:
                        VisualAlert(text: "Registrado no exitoso", style: .errors)
                            .frame(height: 10)
                    case .none:
                        Text("")
                    }
                }
            }
            
        }
    }
}

#Preview {
    RegisterView()
}


