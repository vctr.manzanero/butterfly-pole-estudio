//
//  ChatMessage.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 28/04/24.
//

import SwiftUI
import Combine
import UIKit
import FirebaseDatabaseInternal
import FirebaseStorage



struct ChatMessage: View {
    @State var text: String = ""
    @State var msjs: [Message] = []
    @StateObject var viewModel: ChatViewModel = ChatViewModel()
    var recieber: String
    var name: String
    @State var isShowPicker: Bool = false
    @State var showImg: Bool = false
    @State var image: Image? = Image(systemName: "person.circle.fill")
    
    init(uid: String, name: String) {
        UITableViewCell.appearance().backgroundColor = .clear
        UITableView.appearance().backgroundColor = .clear
        UITableView.appearance().sectionIndexBackgroundColor = .clear
        self.recieber = uid
        self.name = name
    }
    
    var body: some View {
        VStack {
            ZStack {
                Image("backChat")
                    .resizable()
                    .opacity(0.25)
                VStack (spacing: 0) {
                    Spacer()
                    ScrollViewReader { scroll in
                        List (0..<viewModel.listCharUser.count, id: \.self) { indexMsj in
                           
                            MessageClouds(msj: viewModel.listCharUser[indexMsj] )
                            
                        }
                        .onChange(of: viewModel.listCharUser, perform: { value in
                            scroll.scrollTo(value.count - 1)
                        })
                        .scrollContentBackground(.hidden)
                        
                        
                        
                        /*List {
                         ForEach(msjs, id: \.self) { indexMsj in
                         MessageClouds(direction: .right, text: indexMsj)
                         
                         }
                         //.scaleEffect(x: 1, y: -1, anchor: .center)
                         }
                         .autocorrectionDisabled()
                         //.rotationEffect(.degrees(180))
                         //.scaleEffect(x: 1, y: -1, anchor: .center)
                         .scrollContentBackground(.hidden)*/
                        
                        HStack {
                            Button(action: {
                                self.isShowPicker.toggle()
                            }, label: {
                                Image(systemName: "plus")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 25, height: 25)
                            })
                            .sheet(isPresented: $isShowPicker) {
                                ImagePicker(image: self.$image)
                                
                            }
                            .onChange(of: image, perform: { newValue in
                                showImg.toggle()
                            })
                            .padding(5)
                            CustomTextField(custonState: $text, text: "", style: .standar)
                            
                            HStack {
                                if text.isEmpty {
                                    Button(action: {
                                        viewModel.selectPDF()
                                    }, label: {
                                        Image(systemName: "camera")
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 25, height: 25)
                                    })
                                    
                                    .padding(5)
                                    Button(action: {}, label: {
                                        Image(systemName: "mic")
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 25, height: 25)
                                    })
                                    .padding(5)
                                } else {
                                    Button(action: {
                                        viewModel.sendMessage(msj: text, receiber: recieber)
                                        text = ""
                                    }, label: {
                                        Image(systemName: "paperplane.circle.fill")
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 25, height: 25)
                                    })
                                    .padding(5)
                                }
                            }
                        }
                        .padding()
                        
                        .background(.white)
                    }
                }
                if let selectedFile = viewModel.selectedFile {
                    Text("Archivo seleccionado: \(selectedFile.lastPathComponent)")
                        .padding()
                    
                    Button("Subir PDF") {
                        // Lógica para subir el archivo PDF a Firebase Storage
                        viewModel.uploadPDF()
                    }
                    .padding()
                    .disabled(viewModel.isUploading)
                }
                            
                if viewModel.isUploading {
                    ProgressView(value: viewModel.uploadProgress, total: 100)
                        .padding()
                }
            }
            .sheet(isPresented: $showImg) {
                ImageAlertView(image: self.$image, closed: $showImg, viewModel: viewModel, reciber: recieber )
            }
        }
        .onDisappear {
            viewModel.stopObservingMessages()
        }
        .task {
            viewModel.readMessages(uidReceiver: recieber)
        }
        .keyboardManagment()
        .navigationBarTitle("", displayMode: .inline)
        .ignoresSafeArea(edges: .bottom)
        .toolbar {
            ToolbarItemGroup(placement: .topBarLeading) {
                ZStack {
                    RoundedRectangle(cornerRadius: 20)
                        .frame(width: 40, height: 40)
                        .overlay(Circle().stroke(.white, lineWidth: 2))
                        .foregroundStyle(.clear)
                        .shadow(radius: 7)
                    Image(systemName: "person.fill")
                        .resizable()
                        .clipShape(Circle())
                        .frame(width: 30, height: 30)
                }
                Text(name)
                    .padding()
            }
            ToolbarItemGroup(placement: .topBarTrailing) {
                Button(action: {}, label: {
                    Image(systemName: "video.fill")
                    
                })
                Button(action: {}, label: {
                    Image(systemName: "phone")
                    
                })
            }
        }
    }
}

#Preview {
    ChatMessage(uid: "", name: "")
}



struct KeyboardManagment: ViewModifier {
    @State private var offset: CGFloat = 0
    func body(content: Content) -> some View {
        GeometryReader { geo in
            content
                .onAppear {
                    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main) { (notification) in
                        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
                        withAnimation(Animation.easeOut(duration: 0.5)) {
                            
                            offset = keyboardFrame.height - geo.safeAreaInsets.bottom
                        }
                    }
                    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { (notification) in
                        withAnimation(Animation.easeOut(duration: 0.1)) {
                            
                            offset = 0
                        }
                    }
                }
                .padding(.bottom, offset)
        }
    }
}
extension View {
    func keyboardManagment() -> some View {
        self.modifier(KeyboardManagment())
    }
}


