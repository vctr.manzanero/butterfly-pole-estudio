//
//  ChatListCell.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 28/04/24.
//

import SwiftUI

struct ChatListCell: View {
    var chat: ChatModel
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 30)
                    .frame(width: 60, height: 60)
                    .overlay(Circle().stroke(.white, lineWidth: 2))
                    .foregroundStyle(.clear)
                    .shadow(radius: 7)
                Image(systemName: "person.fill")
                    .resizable()
                    .clipShape(Circle())
                    .frame(width: 40, height: 40)
            }
            Text(chat.name)
                .padding()
        }
    }
}

#Preview {
    ChatListCell(chat: ChatModel(name: "Vctr manzanero", hora: "13:01", id: 1, image: ""))
}
