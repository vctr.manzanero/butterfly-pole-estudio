//
//  Chat.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 27/04/24.
//

import SwiftUI

struct ChatList: View {
    
    @StateObject var viewModel: ChatListViewModel = ChatListViewModel()
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(viewModel.list) { chat in
                    NavigationLink(destination:
                                    ChatMessage (uid: chat.id, name: chat.name)
                        .toolbar(.hidden, for: .tabBar)
                                   , label: {
                        ChatListCell(chat: ChatModel(name: chat.name, hora: "12:56", id: 0, image: ""))
                    })
                }
            }
            .navigationTitle("Chats")
        }
        .task {
            await viewModel.getUsersChat()
        }
        .tint(.blue)
    }
}

#Preview {
    ChatList()
    //ChatList()
}
