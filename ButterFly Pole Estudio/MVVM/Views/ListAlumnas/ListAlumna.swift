//
//  ListAlumna.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 12/02/24.
//

import SwiftUI

struct ListAlumna: View {
    
    @State var texto: String = ""
    
    var body: some View {
        TemplateView {
            List {
                ForEach(0..<10) { index in
                    CellAlumna()
                }
            }
        }
    }
}

#Preview {
    ListAlumna()
}

struct ListAlumnaContent: View {
    var body: some View {
        Text("")
    }
}
