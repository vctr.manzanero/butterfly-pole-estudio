//
//  ContentView.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 10/02/24.
//

import SwiftUI

struct Login: View {
    
    @StateObject var viewModel: LoginUserViewModel = LoginUserViewModel()
    @State var user: String = "vctr.31@hotmail.com"
    @State var pass: String = "".textLabel(identifier: .noneString)
    
    var body: some View {
        
        switch viewModel.flow {
        case .firstFlow:
             RegisterView(name: "")
        case .noFirst:
             TabViewApp()
        case .none:
            if viewModel.loading {
                ProgressView("Cargando")
            } else {
                NavigationStack {
                    VStack() {
                        Image(Images.logo.rawValue)
                            .resizable()
                            .frame(width: 200, height: 300)
                            .offset(y: -50)
                        Text("Bienvenidos")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .offset(y: -50)
                        HStack {
                            ImageLogin(nameImage: "person.fill")
                            CustomTextField(custonState: $user, text: "Usuario", style: .standar)
                        }
                        .padding()
                        .offset(y: -50)
                        HStack {
                            ImageLogin(nameImage: "lock")
                            CustomTextField(custonState: $pass, text: "Password", style: .secure)
                        }
                        .padding()
                        .offset(y: -80)
                        Button(action: {
                            viewModel.getLogin(email: user, pass: pass)
                        }, label: {
                            Custom_Buttons(title: "Log in", style: .principal)
                                    .frame(width: 200)
                        })
                        .offset(y: -100)
                        
                        Button(action: {
                            
                        }, label: {
                            Text("Olvide mi contraseña")
                                .foregroundStyle(.black)
                        })
                        .offset(y: -80)
                        HStack {
                            Spacer()
                            Button(action: {
                                
                            }, label: {
                                Text("Contactanos por ayuda")
                                    .foregroundStyle(.black)
                                    .padding()
                            })
                        }
                        Spacer()
                        Text("@ S t u d i o b u t t e r f l y P o l e")
                        if viewModel.noLogin {
                            VisualAlert(text: "Usuario no encontrado", style: .errors)
                        }
                    }
                }
            }
        }
    }
}

struct ImageLogin: View {
    
    var nameImage: String
    
    var body: some View {
        
        Image(systemName: nameImage)
            .resizable()
            .frame(width: 30, height: 30)
            .padding()
            .frame(width: 50, height: 50)
            .background(.princilal)
            .foregroundStyle(.white)
            .clipShape(.circle)
    }
}

#Preview {
    Login()
}
