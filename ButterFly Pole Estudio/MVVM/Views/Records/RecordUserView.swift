//
//  RecordUserView.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 14/02/24.
//

import SwiftUI

struct HeadCell: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.headline)
            .foregroundStyle(.white)
            .frame(width: 100, height: 50)
            .background(.princilal)
            .overlay {
                Rectangle()
                    .stroke(Color.black)
            }
    }
}

struct RowCell: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.subheadline)
            .frame(width: 100, height: 50)
            .overlay {
                Rectangle()
                    .stroke(Color.black)
            }
    }
}

struct RecordUserView: View {
    
    var body: some View {
        TemplateView {
            List {
                HeadTableView()
                    .listRowSeparator(.hidden)
                    .listRowSpacing(0)
                    .listRowInsets(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))
                
                ForEach (0..<10) {_ in 
                    RowTableView()
                        .listRowSeparator(.hidden)
                        .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                }
            }
            .listStyle(.plain)
            .scrollContentBackground(.hidden)
        }
    }
}

#Preview {
    RecordUserView()
}

struct HeadTableView: View {
    var body: some View {
        HStack(spacing: 0) {
            Spacer()
            Text("Fecha")
                .modifier(HeadCell())
            Text("plan")
                .modifier(HeadCell())
            Text("Importe")
                .modifier(HeadCell())
            Spacer()
        }
    }
}

struct RowTableView: View {
    var body: some View {
        HStack(spacing: 0) {
            Spacer()
            Text("12/06/2023")
                .modifier(RowCell())
            Text("Basìca")
                .modifier(RowCell())
            Text("$ 750")
                .modifier(RowCell())
            Spacer()
        }
    }
}
