//
//  addUser.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 13/02/24.
//

import SwiftUI

struct addUser: View {
    
    @StateObject var viewModel: AddUserViewModel = AddUserViewModel()
    
    var body: some View {
        ZStack {
            TemplateView {
                ExtractedView()
            }
            if viewModel.success {
                VisualAlert(text: "Registro Exitoso", style: .succes)
            }
            if viewModel.error {
                VisualAlert(text: "Registro no creado", style: .errors)
            }
        }
        .environmentObject(viewModel)
    }
}

#Preview {
    addUser()
}

struct ExtractedView: View {
   
    @EnvironmentObject var viewModel: AddUserViewModel
    
    @State var name: String = ""
    @State var pass: String = ""
    
    var body: some View {
        VStack (spacing: 0 ) {
            HStack {
                Text("Crear nueva alumna")
                    .font(.title)
                    .padding()
                Spacer()
            }
            CustomForm(stateName: $name, style: .bold, placeHolder: "Nombre")
                .offset(y: -10)
            CustomForm(stateName: $pass, style: .bold, placeHolder: "Contraseña")
                .offset(y: -35)
            HStack {
                Button(action: {
                    viewModel.addUser(email: name, pass: pass)
                }, label: {
                    Custom_Buttons(title: "Crear", style: .principal)
                })
                .frame(width: 150)
                Button(action: {}, label: {
                    Custom_Buttons(title: "Cancelar", style: .principal)
                })
                .frame(width: 150)
            }
            Spacer()
        }
    }
}
