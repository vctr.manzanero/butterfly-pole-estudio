//
//  Calendar.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 11/02/24.
//

import SwiftUI


struct Calendar: View {
    
    @State private var date = Date()
    @Environment(\.calendar) var calendar
    @Environment(\.timeZone) var timeZone
    @State var identifier = "es"
    @StateObject var viewModel = CalendarViewModel()
    @State var element: Bool = false
    
    
    var bounds: PartialRangeFrom<Date> {
        let start = Date()
        
        return start...
        
    }
    
    var body: some View {
        VStack (spacing: 0) {
            DatePicker(
                "Seleccione fecha",
                selection: $date,
                in: bounds, displayedComponents: [.date]
            )
            .datePickerStyle(.automatic)
            .scrollIndicators(.hidden)
            .padding()
            .tint(.blue)
            HStack {
                ZStack {
                    RoundedRectangle(cornerRadius: 40)
                        .frame(width: 80, height: 80)
                        .overlay(Circle().stroke(.white, lineWidth: 2))
                        .foregroundStyle(.clear)
                        .shadow(radius: 7)
                    viewModel.profile
                        .resizable()
                        .clipShape(Circle())
                        .frame(width: 70, height: 70)
                }
                    
                ProfileImage()
            }
            Divider()
            ScrollView {
                if viewModel.loadingClass {
                    ProgressView("Cargando clases")
                } else {
                    ForEach (viewModel.arrayClass) { element in
                        getHoursView(titleToggle: element.name, timeTitle: element.hora, idClass: element.id)
                    }
                }
                Divider()
                Button(action: {
                    viewModel.saveDateClass(date)
                }, label: {
                    Custom_Buttons(title: "".textLabel(identifier: .textSave), style: .principal)
                        .padding()
                })
            }.task {
                viewModel.getClass()
            }
        }
        .environmentObject(viewModel)
        .environment(\.locale, .init(identifier: identifier))
        .onAppear {
            viewModel.getImagen()
        }
    }
}


#Preview {
    Calendar()
}

struct getHoursView: View {
    
    @State var isActivate: Bool = false
    var titleToggle: String
    var timeTitle: String
    var idClass: Int
    
    var body: some View {
        HStack {
            Toggle(titleToggle, isOn: $isActivate)
                .toggleStyle(.switch)
                .tint(.green)
            Text(timeTitle)
                .padding()
                .clipShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)
                                           )
                )
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(.princilal, lineWidth: 2))
        }
        .padding()
    }
}

struct ProfileImage: View {
    
    @EnvironmentObject var viewModel: CalendarViewModel
    
    var body: some View {
        VStack {
            Text(viewModel.dataUser.name)
                .font(.headline)
                .fontWeight(.bold)
            Text("Mensualidad Básica - \(viewModel.dataUser.cretid)hrs restantes")
                .font(.subheadline)
                .fontWeight(.light)
            Text("2 hrs (Reservadas)")
                .font(.subheadline)
                .fontWeight(.ultraLight)
        }
        .task {
            viewModel.getInfoUser()
        }
        .padding()
    }
}
