//
//  Calendar.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 10/02/24.
//

import SwiftUI

struct TabViewApp: View {
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor.princilal
    }
    
    @State var isTabViewShown = true
    @State var seleccions: Int = 3
    
    var body: some View {
        
        TabView(selection: $seleccions ) {
            if isTabViewShown {
                Group {
                    Calendar()
                        .tabItem {
                            VStack {
                                Image(systemName: "calendar")
                                    .renderingMode(.template)
                                    .foregroundColor(Color.white)
                                Text("Calendarios")
                            }
                        }
                        .tag(0)
                    Text("Reglas")
                        .tabItem {
                            VStack {
                                Image(systemName: "note.text")
                                    .renderingMode(.template)
                                    .foregroundStyle(.white)
                                Text("Reglas")
                            }
                        }
                        .tag(1)
                    Text("Mensualidades")
                        .tabItem {
                            VStack {
                                Image(systemName: "dollarsign.arrow.circlepath")
                                Text("Mensualidades")
                            }
                        }
                        .tag(2)
                    Text("Perfil")
                        .tabItem {
                            VStack {
                                Image(systemName: "person.circle")
                                Text("Perfil")
                            }
                        }
                        .tag(4)
                    
                    ChatList()
                        .tabItem {
                            VStack {
                                Image(systemName: "message")
                                Text("Chat")
                            }
                        }
                        .tag(3)
                }
            }
        }
        .tint(.white)
    }
}

#Preview {
    TabViewApp()
}
