//
//  UseCaseFireStore.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 15/02/24.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage

class UseCaseFireStore: uploadFireStore {
    
    let storage = Storage.storage()
    
    func upload(image: UIImage,
                success: @escaping () -> Void,
                errorResponse: @escaping () -> Void,
                completion: @escaping () -> Void) {
        let uid = UserDefaults.standard.string(forKey: "idUser") ?? "123"
        let storageRef = storage.reference().child("profiles/\(uid).jpg")
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpg"
        if let data = image.jpegData(compressionQuality: 0.051) {
                storageRef.putData(data, metadata: metadata) { (metadata, error) in
                        if let _ = error {
                            errorResponse()
                        }

                        if let _ = metadata {
                            success()
                        }
                }
        }
        completion()
    }
    
    func uploadImageChat(image: UIImage,
                success: @escaping (String) -> Void,
                errorResponse: @escaping () -> Void,
                completion: @escaping () -> Void) {
        let uid = UUID().uuidString
        let storageRef = storage.reference().child("imageChat/\(uid).jpg")
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpg"
        if let data = image.jpegData(compressionQuality: 0.1) {
                storageRef.putData(data, metadata: metadata) { (metadata, error) in
                        if let _ = error {
                            errorResponse()
                        }

                        if let _ = metadata {
                            success(uid)
                        }
                }
        }
        completion()
    }
    
    func insertStuden(data: [String], 
                      success: @escaping () -> Void,
                      errorResponse: @escaping () -> Void) {
        let db = Firestore.firestore()
        
        Auth.auth().currentUser?.updatePassword(to: data[2]) { errorPass in
            if let _ = errorPass {
                errorResponse()
            }
            else {
                db.collection("studen").addDocument(data: ["uid": UserDefaults.standard.string(forKey: "idUser") ?? "",
                                                           "name": data[0],
                                                           "email": data[1],
                                                           "pass":data[2],
                                                           "birstDay": data[3],
                                                           "phone": data[4]
                                                          ]
                )
                { (errors) in
                    if let _ = errors {
                        errorResponse()
                    } else {
                        db.collection("users").document(UserDefaults.standard.string(forKey: "sesion") ?? "").updateData(["firstTime": false])
                        success()
                    }
                }
            }
        }
    }
}
