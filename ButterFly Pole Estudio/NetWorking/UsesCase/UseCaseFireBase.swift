//
//  UseCaseFireBase.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 13/02/24.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore
import FirebaseFunctions


class  UseCaseFireBase: GetData {
    
    func addUser(email: String, pass: String, success: @escaping () -> Void, error: @escaping () -> Void) {
        
        Auth.auth().createUser  (withEmail: email, password: pass) { authResult, errors in
            authResult?.user.sendEmailVerification()

            if errors != nil {
                error()
            } else  {
                let db = Firestore.firestore()
                
                db.collection("users").addDocument(data: ["firstTime": true, "rol":3, "uid": authResult?.user.uid ?? "" ]) { (errors) in
                    if let _ = errors {
                        error()
                    }
                }
                success()
            }
        }
    }
    
    func getListUsers() async -> [CountsUser]{
        let db = Firestore.firestore()
        var listUsersChat:[CountsUser] = []
        
        do {
            let querySnapshot = try await db.collection("users").whereField("uid", isNotEqualTo: UserDefaults.standard.string(forKey: "idUser")!)
                .getDocuments()
            for document in querySnapshot.documents {
                if let id = document.data()["uid"], let email = document.data()["email"], let name = document.data()["firstname"] {
                    listUsersChat.append(CountsUser(email: email as! String, name: name as! String, id: id as! String))
                }
            }
            
        } catch {
            print("Error getting documents: \(error)")
        }
        return listUsersChat
    }
    
}

extension UseCaseFireBase: getLogin {
    func getInfoUser(uid: String) async -> Int{
        
        let db = Firestore.firestore()
        
        do {
            let querySnapshot = try await db.collection("users").whereField("uid", isEqualTo: UserDefaults.standard.string(forKey: "idUser")!)
                .getDocuments()
            for document in querySnapshot.documents {
                UserDefaults.standard.set(document.documentID, forKey: "sesion")
                if let first = document.data()["firstTime"] {
                    return first as! Int
                }
            }
        } catch {
            print("Error getting documents: \(error)")
        }
        
        return 0
    }
    
    func loginUser(email: String, pass: String, success: @escaping (String) -> Void, error: @escaping () -> Void) {
        Auth.auth().signIn(withEmail: email, password: pass) { (result, errors) in
            if errors != nil {
                error()
            }
            else {
                if let uid = result?.user.uid {
                    UserDefaults.standard.set(uid, forKey: "idUser")
                    success(uid)
                }
            }
        }
    }
}

extension UseCaseFireBase: getInfoUser {
    func getUser(data: String, 
                 success: @escaping (User) -> Void,
                 errorResponse: @escaping () -> Void) async {
        
        let db = Firestore.firestore()
        
        do {
            let querySnapshot = try await db.collection("studen").whereField("uid", isEqualTo: data)
                .getDocuments()
            for document in querySnapshot.documents {
               let obj = User(birtsDay: document.data()["birstDay"] as! String,
                            name: document.data()["name"] as! String,
                            email: document.data()["email"] as! String,
                            phone: document.data()["phone"] as! String,
                            uid: document.data()["uid"] as! String,
                            cretid: document.data()["credits"] as! Int)
                success(obj)
            }
        } catch {
            print("Error getting documents: \(error)")
        }
        
    }
}

extension UseCaseFireBase: GetclassPole {
    func GetclassPole(success: @escaping ([PoleClass]) -> Void, errorResponse: @escaping () -> Void, completion: @escaping () -> Void) async {
        let db = Firestore.firestore()
        do {
            let querySnapshot = try await db.collection("clases")
                .getDocuments()
            var response: [PoleClass] = []
            for document in querySnapshot.documents {
               let obj = PoleClass(name: document.data()["name"] as! String,
                                   hora: document.data()["hora"] as! String,
                                   id: document.data()["id"] as! Int, isSelect: false)
                response.append(obj)
            }
            success(response)
            completion()
        } catch {
            print("Error getting documents: \(error)")
            errorResponse()
        }
    }
}
