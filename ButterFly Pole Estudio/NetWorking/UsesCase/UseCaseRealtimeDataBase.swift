//
//  UseCaseRealtimeDataBase.swift
//  ButterFly Pole Estudio
//
//  Created by victor manzanero on 27/04/24.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import UIKit

class UseCaseRealtimeDataBase {
    
    var ref: DatabaseReference = Database.database().reference()

    func sendMessage(newMessageText: String, idChat: String, url: String = "", type: msjType = .text) {
            guard let user = Auth.auth().currentUser else { return }
            let message = ["text": newMessageText,
                           "senderID": user.uid,
                           "isRead": false,
                           "receiver": idChat,
                           "url": url,
                           "type": "txt",
                           "timestamp": Date().timeIntervalSince1970] as [String : Any]
            ref.child("messages").childByAutoId().setValue(message)
           
    }
    
    func sendMessageImage(idChat: String, type: msjType = .image , image: UIImage) {
        let firestore: UseCaseFireStore = UseCaseFireStore()
        firestore.uploadImageChat(image: image, success: { url in
            guard let user = Auth.auth().currentUser else { return }
            let message = ["text": "",
                           "senderID": user.uid,
                           "isRead": false,
                           "receiver": idChat,
                           "url": url,
                           "type": "image",
                           "timestamp": Date().timeIntervalSince1970] as [String : Any]
            
            self.ref.child("messages").childByAutoId().setValue(message)
        }, errorResponse: {
            
        }, completion: {
            
        })
           
    }
    
}
